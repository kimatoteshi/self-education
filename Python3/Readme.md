
-----------------
[USEFUL COMMANDS]
-----------------
$ pyenv versions
$ pyenv virtualenvs
$ pyenv virtualenv-delete -f "${PWD##*/}"_env
---------------------------------
[Create&Init git repo CHEATSHEET]
---------------------------------
$ PASSWORD=<PASSWORD> 
$ curl -X POST -v -u kimatoteshi@gmail.com: -H 'Content-Type:application/json' https://api.bitbucket.org/2.0/repositories/kimatoteshi/awasad --data '{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks" }'
$ curl -X DELETE -v -u kimatoteshi@gmail.com: https://api.bitbucket.org/2.0/repositories/kimatoteshi/awasad
$ echo ".git" > .gitignore && \
	git add . && \
	git commit -m "Initial commit" && \
	git remote add bitbucket https://kimatoteshi@bitbucket.org/kimatoteshi/awasad.git && \ 
	git push -u bitbucket master
--------------------------------------
Example DATASETS (in different types)
https://kaggle.com/datasets
--------------------------------------
pip freeze | xargs pip uninstall -y  