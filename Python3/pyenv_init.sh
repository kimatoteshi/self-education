#!/bin/sh
VER=$1
if [ $# -eq 0 ]
  then
    echo "Usage:" 
    echo "	$ ./pyenv_init.sh [Python_version]"
	echo "" 
    echo "Example:" 
    echo "	$ ./pyenv_init.sh 3.7.5"
    exit
fi

brew update
brew install pyenv
brew upgrade pyenv
# Refer to https://github.com/pyenv/pyenv#basic-github-checkout to setup inits and autocomplete
# To install required version of python 
pyenv install --skip-existing $VER
# pyenv versions
# To uninstall 
# pyenv uninstall -f 3.7.5

# Install pyenv-virtualenv plugin
brew install pyenv-virtualenv
brew upgrade pyenv-virtualenv
# Create named pyenv 
pyenv virtualenv $VER "${PWD##*/}"_env
pyenv local "${PWD##*/}"_env
pip install --upgrade pip
echo 'cat Readme.md:'
echo '' > Readme.md 
echo '-----------------' >> Readme.md 
echo '[USEFUL COMMANDS]' >> Readme.md 
echo '-----------------' >> Readme.md 
echo '$ pyenv versions' >> Readme.md 
echo '$ pyenv virtualenvs' >> Readme.md 
echo '$ pyenv virtualenv-delete -f "${PWD##*/}"_env' >> Readme.md 
echo '---------------------------------' >> Readme.md 
echo '[Create&Init git repo CHEATSHEET]' >> Readme.md 
echo '---------------------------------' >> Readme.md 
echo "$ read -s PASSWORD " >> Readme.md 
echo "$ curl -X POST -v -u kimatoteshi@gmail.com:\${PASSWORD} -H 'Content-Type:application/json' https://api.bitbucket.org/2.0/repositories/kimatoteshi/`echo ${PWD##*/} | tr '[:upper:]' '[:lower:]'` --data '{\"scm\": \"git\", \"is_private\": \"true\", \"fork_policy\": \"no_public_forks\" }'" >> Readme.md 
echo "$ curl -X DELETE -v -u kimatoteshi@gmail.com:\${PASSWORD} https://api.bitbucket.org/2.0/repositories/kimatoteshi/`echo ${PWD##*/} | tr '[:upper:]' '[:lower:]'`" >> Readme.md 
echo '$ echo ".git" > .gitignore && \' >> Readme.md  
echo '	git init && \' >> Readme.md 
echo '	git add . && \' >> Readme.md 
echo '	git commit -m "Initial commit" && \' >> Readme.md 
echo "	git remote add bitbucket https://kimatoteshi@bitbucket.org/kimatoteshi/`echo ${PWD##*/} | tr '[:upper:]' '[:lower:]'`.git && \ " >> Readme.md 
echo '	git push -u bitbucket master' >> Readme.md 
echo '--------------------------------------' >> Readme.md 
echo 'Example DATASETS (in different types)' >> Readme.md 
echo 'https://kaggle.com/datasets' >> Readme.md 
cat Readme.md