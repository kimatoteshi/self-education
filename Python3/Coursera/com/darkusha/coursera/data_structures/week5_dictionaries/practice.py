import time
start_time = time.time()

# # todo 1
# mydict = {}
# mydict = dict()
#
# mydict['Apple'] = 0;
# mydict['Pineapple'] = 0;
# mydict['Plumbs'] = 0;
#
# # print(mydict['Apple'])
# # print(mydict)
#
# fruits = ['Apple', 'Pickle', 'Banana', 'Banana','Banana','Plumbs']
# for item in fruits:
#     mydict[item] = mydict.get(item,0) + 1
# print(mydict)



# # todo 2
# mydict = {}
# mydict = dict()
#
# mydict['Apple'] = 0;
# mydict['Pineapple'] = 0;
# mydict['Plumbs'] = 0;

# print(mydict['Apple'])
# print(mydict)

# fruits = ['Apple', 'Pickle', 'Banana', 'Banana','Banana','Plumbs']
# for item in fruits:
#     mydict[item] = mydict.get(item,0) + 1
# print(mydict)
# print(list(mydict))
# print(mydict.items())
# for key,value in mydict.items():
#     print(key, value)



# # todo PRACTICE
name = input("Enter file:")
if len(name) < 1: name = "mbox-short.txt"
handle = open(name)

counts = dict()
for line in handle:
    # if not line.startswith("From: ") : continue
    # line = line.replace("From: ","")
    words = line.split()
    for word in words:
        word = word.upper()
        counts[word] = counts.get(word,0) + 1

# for key,value in counts.items():
#     print('\''+key+'\' : ', value)

for w in sorted(counts.items(), key=lambda x: x[1], reverse=True):
    print(w)

bigestcount = None
bigestword = None
for word,count in counts.items():
    if bigestcount is None or count > bigestcount:
        bigestword = word
        bigestcount = count

print(bigestword, bigestcount)
print("Number of words : %s" % len(counts))
print("--- %s seconds ---" % (time.time() - start_time))
