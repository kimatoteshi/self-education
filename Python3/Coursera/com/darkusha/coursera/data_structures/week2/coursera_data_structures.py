# https://www.coursera.org/learn/python-data/home/welcome

# TODO 1 slide
# str1 = "Hello"
# str2 = 'there'
# bob = str1 + str2
# print(bob)
#
# str3 = '123'
# str3 = int(str3) + 1
#
# print(str3)

# TODO 2 slide
# name = input("Enter name:")
# apple = input("Applce mum:")
# print(name)
# print(int(apple)+1)

# TODO 3 slide
# fruit = "banana"
# print(len(fruit))

# TODO 4 slide
# fruit = "banana"
# index = 0
# while index < len(fruit):
#     letter = fruit[index]
#     print(index, letter)
#     index += 1

# TODO 5 slide
# fruit = "banana"
# for letter in fruit:
#     print(letter)

# TODO 6 slide OWN PLAY
# fruit = "banana"
# count = 0
# for letter in fruit:
#     print(str(count) +" "+ letter)
#     count += 1

# TODO !!! FOR LOOP
# TODO 6 slide OWN PLAY
# fruit = "banana"
# count = 0
# for letter in fruit:
#     if  letter == 'a':
#         count += 1
# print(str(count))


# # TODO 7 !!! SLICING
# s = "Monty Python"
# print(s[0:4])
# print(s[6:7])
# print(s[6:20])

text = "X-DSPAM-Confidence:    0.8475"
pos = text.find(":")
res = text[pos+1:len(text)]
res = float(res.strip())
print(res)


# # TODO 8 !!! LOGICAL OPERATORS
# s = "banana"
# print('an' in s)

# todo TYPE() DUR()
# s = "banana"
# print(type(s))
# print(dir(s))


# todo TRIMS: LSTRIP(), RSTRIP()
# s = "           banana         !                    "
# print(s)
# print(len(s))
# s = s.lstrip()
# s = s.rstrip()
### s =s.strip() #lstrip()+ rtrip()
# print(s)
# print(len(s))

# data = 'From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008'
# pos = data.find('.')
# print(pos)
# print(data[pos:pos+3])