# # todo TUPLES ARE IMUTABLE!!
# # todo TUPLES ARE MORE EFFICIENT THAN LISTS (Less memory use + performance)
# mylist = list()
# mytuple = tuple()
# mylist = ['1','2','3']
# mytuple= ('1','2','3')
# mylist[2] = 'test'
# # mytuple[2] = 2 //EXCEPTION : TypeError: 'tuple' object does not support item assignment
#
# print(mylist)
# print(mytuple)
# print(max(mylist))
# print(max(mytuple))

# dir(mylist) # fixme WHY IT'S NOT WORKING IN IDE
# dir(mytuple) # fixme WHY IT'S NOT WORKING IN IDE

# (x,y) = (4, "Fred")
# print(y)

# # todo
# print((1,4,3)<(2,2,4))

# # todo SORT BY KEY
# d = {'a':10, 'b':12, 'c':3, 'c':4}
# print(sorted(d.items()))
# for k,v in sorted(d.items()):
#     print(k,v)

# # todo SORT BY VALUE
# d = {'a':10, 'b':12, 'c':3, 'c':4}
# tmpd = list()
# for k,v in sorted(d.items()):
#     tmpd.append((v,k))
# tmpd = sorted(tmpd,reverse=True)
# print(tmpd)


# # todo JUST PLAY
# fhand = open('romeo.txt')
# counts = dict()
# for line in fhand:
#     words = line.split()
#     for word in words:
#         counts[word] = counts.get(word, 0) + 1
#
# lst = list()
# # # for k,v in counts.items():
# # #     newtup = (v,k)
# # #     # print(newtup)
# # #     lst.append(newtup)
# # #     # print(lst)
# # #
# # # lst = sorted(lst, reverse=True)
# # # for k,v in lst[:10]:
# # #     print(v,k)
# # # todo BLOCK ABOVE MIGHT BE SIMPLY SHORTENED

# lst = sorted([ (v,k) for k,v in counts.items()])
# lst = sorted(lst, reverse=True)
# for k,v in lst[:10]:
#     print(k,v)


# # todo PRACTICE
# handle = open('mbox-short.txt')
#
# counts = dict()
# for line in handle:
#     if not line.startswith("From "): continue
#     word = line.split()[5][:2]
#     counts[word] = counts.get(word, 0) + 1
# for k,v in sorted(counts.items()):
#     print(k,v)

# # todo SOCKET CONNECTION
# import socket
# mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# mysock.connect(('data.pr4e.org', 80))
# cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\n\n'.encode()
# mysock.send(cmd)
#
# while True:
#     data = mysock.recv(512)
#     if(len(data) < 1 ): break
#     print(data.decode())
# mysock.close()