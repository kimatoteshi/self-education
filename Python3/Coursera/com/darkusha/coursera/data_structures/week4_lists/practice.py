friends = ['Someone', 'Denis A', 'Denis K']
carryon = ['socks', 'shirt', 'perfume']

# # todo
# print(type(friends))

# # todo
# for i in friends:
#     print(i)

# # todo
# print(range(len(friends)))

# # todo
# for i in range(len(friends)):
#     print(friends[i])

# # todo
# print(friends[:])


# # todo new list
# mylist = list()
# mylist.append(1)
# mylist.append("string")
# mylist.append(3.14)
# print(mylist)

# # todo IN operator
# print('Denis' in friends)

# # todo SORT() method
# print(friends.sort()) # # Fixme INVALID - returns None
# friends.sort()
# print(friends)


# # todo PART 2
# # todo WHILE LOOP
# mylist = []
# mylist1 = list()
#
# while True:
#     inp = input('Enter number: ')
#     if inp == 'done' : break
#     mylist.append(float(inp))
# print(sum(mylist) / len(mylist))


# # todo PART 3
# # todo List and STRINGS
# words = 'theese three words'
# print(words)
# print(words.split())

# # todo PART 3
# # todo PRACTICE
# fname = input("Enter file name: ")
# fh = open(fname)
fh = open('romeo.txt')
lst = list()
for line in fh:
    words = line.rstrip().split()
    for word in words:
        if word not in lst:
            lst.append(word)
lst.sort()
print(lst)

# # todo PART 4
# # todo PRACTICE
# fh = open("mbox-short.txt")
# count = 0
# for line in fh:
#     if line.startswith('From '):
#         count +=1
#         print(line.split()[1])
# print("There were", count, "lines in the file with From as the first word")