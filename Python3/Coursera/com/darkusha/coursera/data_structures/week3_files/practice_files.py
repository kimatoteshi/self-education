############# todo FILES READING

# todo 1 Slide
file = open('practice_files.py','r')
count = 0
for line in file:
    count += 1
    print(line)
print(count)

# todo 2 Slide
file = open('practice_files.py','r')
inp = file.read()
inp = file.read()
inp = file.read()
print(len(inp))
print(inp[:14])


# todo 3 Slide
file = open('practice_files.py','r')
for line in file:
    line = line.rstrip()
    if line.startswith('# inp '):
        print(line)


# todo 4 Stylistic difference to #todo 3
file = open('practice_files.py','r')
for line in file:
    line = line.rstrip()
    if not line.startswith('# inp '):
        continue
    print(line)

# # todo 5
file = open('practice_files.py','r')
for line in file:
    if 'mp = fi' in line:
        line = line.rstrip()
        print(line)

# todo 6 TRY CATCH : filename : practice_files.py
filename = input('Enter filename: ')
linenumber = 0
try:
    file = open(filename,'r')
    for line in file:
        linenumber += 1
        if 'mp = fi' in line:
            line = line.rstrip()
            # print(line)
            print(str(linenumber) + ":" + line)
except:
    print('There is no file with such name')
    # todo THE QUIT IN CASE of exception
    quit()
print(1)



# todo Practical excercise : filename : practice_files.py
fname = input("Enter file name: ")
try:
    fh = open(fname,'r')
    for line in fh:
        if 'mp = fi' in line:
            line = line.rstrip()
            print(line.upper())
except:
    print('There is no file with such name')
    quit()



# # todo Practical excercise 2 : filename : mbox-short.txt
fname = input("Enter file name: ")
num_of_entries = 0
total_count = float(0)
try:
    fh = open(fname,'r')
    for line in fh:
        if line.startswith('X-DSPAM-Confidence:'):
            num_of_entries += 1
            total_count += float(line.rstrip().replace("X-DSPAM-Confidence: ",""))
except:
    print('There is no file with such name')
    quit()
print("Average spam confidence: " + str(total_count/num_of_entries))
