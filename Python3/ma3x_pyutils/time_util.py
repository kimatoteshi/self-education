import os, time

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) )
        else:
            print('%r  %2.2f s' % \
                  (method.__name__, (te - ts) ))
        return result
    return timed

@timeit
def exec_cmd(*args):
    res = os.popen(*args).read()
    print(res)

exec_cmd("sleep 2 && cat ~/Downloads/Withdr.png | openssl enc -e -aes-256-cbc -a -salt -k 1")