# todo ID()
x = [1,2,3]
print(id(x)) # 4368415240
print(id([1,2,3])) #4560162720


# todo IS()
x = [1,2,3]
y = x
print(y is x)
x.append(4)
print (y)
print(y is x)

# todo TYPE()

x = [1,2,3]
type(x)
type(4)

dir(type(type(x)))

# Immutable                   Mutable
# --------------------------|--------------------------
# int                       |
# float                     |
# complex                   |
# bool                      |
#                           |
# string                    | list
# tuple                     | dict
# frozenset                 | set


# todo Positional Arguments
# printab(a=10, b=20)
# printab(10, b=20)
# printab(b=20, a=10)


# lst = [10,20]
# printab(*lst) № # printab(lst[0],lst[1])
#
# args = {'a':10,'b':20}
# printab(**args) # =# printab(key1=args[key],key2=args[key])
#
# -------------
# printab(a,b, *args)


# todo 1.1
n = int(input())
lst = list()
while (n != 0):
    x = int(input())
    lst.append(x)
    n -= 1
print(sum(lst))

# todo 1.2
x = [1, 2, 3]
y = x
y.append(4)

s = "123"
t = s
t = t + "4"

print(str(x) + " " + s)

# # # todo 1.3
# ans = 0
# for obj in objects: # доступная переменная objects
#     ans += 1
#
# print(ans)