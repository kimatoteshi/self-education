"""IT's a module level
    multi-line comment"""

# TODO LIST FUNCTIONS
movies = ["Matrix",
          "Pulp fiction",
          "Casanova"]
movies.append("Test")
e = movies.pop()
print(e)
movies.extend(["Test2","Test3"])
print(movies)
movies.remove("Test2")
movies.remove("Test3")
print(movies)

movies.insert(1, 1996)
movies.insert(3, 1986)
movies.insert(5, 1980)
print(movies)

# TODO Iterate over list
for movie in movies:
    print(movie)

# TODO THIS IS THE RECUR TRICK!!
def print_lol(the_list):
    """IT's a function level
        multi-line comment"""
    for each_item in the_list:
        if isinstance(each_item, list):
            print_lol(each_item)
        else:
            print(each_item)

print_lol(movies)