```
### -------------------------------------------------------------------
### Binary files encryption
### -------------------------------------------------------------------
cat Attempt.mp3 |openssl enc -e -aes-256-cbc -a > enc.mp3
cat enc.mp3|openssl base64 -d|openssl enc -d -aes-256-cbc > dec.mp3

# ETH Wallet
# ----------
# 0x30e12E3Dc5eDE2c52F5FdC6e77dd1E9a08F3F3BF
# ETH Passphrase
# --------------
# echo "U2FsdGVkX18ZXiQhP6ZmGVvOsa1P59ocIrLq+Jsz/uOB5wkj8gDPHcRcPwQUIPc0
INwmXlb94RjUuF+ywW1ZTBE3TVieYF+atF1DRGuo0nsu6QIdWeIqFPYxRyr5Q5aC
AsSGS4U2+xYAVESTjR4+SQ==" | openssl base64 -d|openssl enc -d -aes-256-cbc

### -------------------------------------------
### Install RDP on Ubuntu EC2
### -------------------------------------------
sudo apt-get update -y 
sudo apt-get install lxde -y && sudo apt-get install xrdp -y && sudo passwd ubuntu

### -------------------------------------------
### Install RDP on Ubuntu EC2
### -------------------------------------------
sudo apt-get upgrade -y  
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update -y 
sudo apt install docker-ce
sudo apt-get upgrade -y
# sudo docker run hello-world
wget https://raw.githubusercontent.com/golemfactory/golem/develop/Installer/Installer_Linux/install.sh
chmod +x install.sh && ./install.sh
sudo usermod -a -G docker $USER
### Relogin to apply the user restriction
golemapp --mainnet
### To start in the background
nohup golemapp --mainnet &

### In another terminal window
golemcli terms show && golemcli terms accept
golemcli account unlock
golemcli -i 

### -------------------------------------------
### Install Golem on OSX
### -------------------------------------------
brew tap homebrew/cask-cask
```