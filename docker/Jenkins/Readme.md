# Autoregistering of 

[![Build Status](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

```docker login```

ma3xdedu111ka:m..b...

## 1. Start docker Jenkins with admin:admin password

```docker run -u root -d -p 8080:8080 -e JAVA_OPTS="-Djenkins.install.runSetupWizard=false" -e JENKINS_OPTS="--argumentsRealm.passwd.admin=admin --argumentsRealm.roles.user=admin --argumentsRealm.roles.admin=admin" -v /Users/darkusha/Downloads/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkinsci/blueocean```

### 1.1 Or regular Start 

docker run -u root -d -p 8080:8080 -v /Users/darkusha/Downloads/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkins/jenkins:2.60.3-alpine

```docker run -u root -d -p 8080:8080 -v /Users/darkusha/Downloads/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkinsci/blueocean```

## 2 Wait till startup

## 3. Activate Jenkins
```jenkins_pass=$(docker exec -i jenkins sh -c 'cat /var/jenkins_home/secrets/initialAdminPassword')```



## 4. Setup butler 

## 5. Install butler 

https://github.com/mlabouardy/butler

```wget https://s3.us-east-1.amazonaws.com/butlercli/1.0.0/osx/butler```

```chmod +x butler```

```mv butler /usr/local/bin/```

## 6. Restore your data with butler

```butler plugins export/import --server localhost:8080 --username admin --password admin```

```butler jobs export/export --server localhost:8080 --skip-folder```


