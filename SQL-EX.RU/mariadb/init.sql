DROP DATABASE IF EXISTS aero;
CREATE DATABASE aero;
USE aero;

drop table IF EXISTS pass_in_trip;
drop table IF EXISTS passenger;
drop table IF EXISTS trip;
drop table IF EXISTS company;


CREATE TABLE Company (
	ID_comp int NOT NULL ,
	name char (10) NOT NULL 
);


CREATE TABLE Pass_in_trip (
	trip_no int NOT NULL ,
	date datetime NOT NULL ,
	ID_psg int NOT NULL ,
	place char (10) NOT NULL 
);


CREATE TABLE Passenger (
	ID_psg int NOT NULL ,
	name char (20) NOT NULL 
);


CREATE TABLE Trip (
	trip_no int NOT NULL ,
	ID_comp int NOT NULL ,
	plane char (10) NOT NULL ,
	town_from char (25) NOT NULL ,
	town_to char (25) NOT NULL ,
	time_out datetime NOT NULL ,
	time_in datetime NOT NULL 
);


ALTER TABLE Company  ADD 
	CONSTRAINT PK2 PRIMARY KEY
	(
		ID_comp
	);   


ALTER TABLE Pass_in_trip  ADD 
	CONSTRAINT PK_pt PRIMARY KEY  CLUSTERED 
	(
		trip_no,
		date,
		ID_psg
	);   


ALTER TABLE Passenger  ADD 
	CONSTRAINT PK_psg PRIMARY KEY  CLUSTERED 
	(
		ID_psg
	);   


ALTER TABLE Trip  ADD 
	CONSTRAINT PK_t PRIMARY KEY  CLUSTERED 
	(
		trip_no
	);   


ALTER TABLE Pass_in_trip ADD 
	CONSTRAINT FK_Pass_in_trip_Passenger FOREIGN KEY 
	(
		ID_psg
	) REFERENCES Passenger (
		ID_psg
	);

ALTER TABLE Pass_in_trip ADD 
	CONSTRAINT FK_Pass_in_trip_Trip FOREIGN KEY 
	(
		trip_no
	) REFERENCES Trip (
		trip_no
	);


ALTER TABLE Trip ADD 
	CONSTRAINT FK_Trip_Company FOREIGN KEY 
	(
		ID_comp
	) REFERENCES Company (
		ID_comp
	);

                                                                                                                                                                                                                                                                 
/*----Company------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Company values(1,'Don_avia  ');
insert into Company values(2,'Aeroflot  ');
insert into Company values(3,'Dale_avia ');
insert into Company values(4,'air_France');
insert into Company values(5,'British_AW');


                                                                                                                                                                                                                                                                 
/*----Passenger------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Passenger values(1,'Bruce Willis        ');
insert into Passenger values(2,'George Clooney      ');
insert into Passenger values(3,'Kevin Costner       ');
insert into Passenger values(4,'Donald Sutherland   ');
insert into Passenger values(5,'Jennifer Lopez      ');
insert into Passenger values(6,'Ray Liotta          ');
insert into Passenger values(7,'Samuel L. Jackson   ');
insert into Passenger values(8,'Nikole Kidman       ');
insert into Passenger values(9,'Alan Rickman        ');
insert into Passenger values(10,'Kurt Russell        ');
insert into Passenger values(11,'Harrison Ford       ');
insert into Passenger values(12,'Russell Crowe       ');
insert into Passenger values(13,'Steve Martin        ');
insert into Passenger values(14,'Michael Caine       ');
insert into Passenger values(15,'Angelina Jolie      ');
insert into Passenger values(16,'Mel Gibson          ');
insert into Passenger values(17,'Michael Douglas     ');
insert into Passenger values(18,'John Travolta       ');
insert into Passenger values(19,'Sylvester Stallone  ');
insert into Passenger values(20,'Tommy Lee Jones     ');
insert into Passenger values(21,'Catherine Zeta-Jones');
insert into Passenger values(22,'Antonio Banderas    ');
insert into Passenger values(23,'Kim Basinger        ');
insert into Passenger values(24,'Sam Neill           ');
insert into Passenger values(25,'Gary Oldman         ');
insert into Passenger values(26,'Clint Eastwood      ');
insert into Passenger values(27,'Brad Pitt           ');
insert into Passenger values(28,'Johnny Depp         ');
insert into Passenger values(29,'Pierce Brosnan      ');
insert into Passenger values(30,'Sean Connery        ');
insert into Passenger values(31,'Bruce Willis        ');
insert into Passenger values(37,'Mullah Omar         ');



                                                                                                                                                                                                                                                                 
/*----Trip------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Trip values(1100,4,'Boeing    ','Rostov                   ','Paris                    ','1900-01-01 14:30:00','1900-01-01 17:50:00');
insert into Trip values(1101,4,'Boeing    ','Paris                    ','Rostov                   ','1900-01-01 08:12:00','1900-01-01 11:45:00');
insert into Trip values(1123,3,'TU-154    ','Rostov                   ','Vladivostok              ','1900-01-01 16:20:00','1900-01-01 03:40:00');
insert into Trip values(1124,3,'TU-154    ','Vladivostok              ','Rostov                   ','1900-01-01 09:00:00','1900-01-01 19:50:00');
insert into Trip values(1145,2,'IL-86     ','Moscow                   ','Rostov                   ','1900-01-01 09:35:00','1900-01-01 11:23:00');
insert into Trip values(1146,2,'IL-86     ','Rostov                   ','Moscow                   ','1900-01-01 17:55:00','1900-01-01 20:01:00');
insert into Trip values(1181,1,'TU-134    ','Rostov                   ','Moscow                   ','1900-01-01 06:12:00','1900-01-01 08:01:00');
insert into Trip values(1182,1,'TU-134    ','Moscow                   ','Rostov                   ','1900-01-01 12:35:00','1900-01-01 14:30:00');
insert into Trip values(1187,1,'TU-134    ','Rostov                   ','Moscow                   ','1900-01-01 15:42:00','1900-01-01 17:39:00');
insert into Trip values(1188,1,'TU-134    ','Moscow                   ','Rostov                   ','1900-01-01 22:50:00','1900-01-01 00:48:00');
insert into Trip values(1195,1,'TU-154    ','Rostov                   ','Moscow                   ','1900-01-01 23:30:00','1900-01-01 01:11:00');
insert into Trip values(1196,1,'TU-154    ','Moscow                   ','Rostov                   ','1900-01-01 04:00:00','1900-01-01 05:45:00');
insert into Trip values(7771,5,'Boeing    ','London                   ','Singapore                ','1900-01-01 01:00:00','1900-01-01 11:00:00');
insert into Trip values(7772,5,'Boeing    ','Singapore                ','London                   ','1900-01-01 12:00:00','1900-01-01 02:00:00');
insert into Trip values(7773,5,'Boeing    ','London                   ','Singapore                ','1900-01-01 03:00:00','1900-01-01 13:00:00');
insert into Trip values(7774,5,'Boeing    ','Singapore                ','London                   ','1900-01-01 14:00:00','1900-01-01 06:00:00');
insert into Trip values(7775,5,'Boeing    ','London                   ','Singapore                ','1900-01-01 09:00:00','1900-01-01 20:00:00');
insert into Trip values(7776,5,'Boeing    ','Singapore                ','London                   ','1900-01-01 18:00:00','1900-01-01 08:00:00');
insert into Trip values(7777,5,'Boeing    ','London                   ','Singapore                ','1900-01-01 18:00:00','1900-01-01 06:00:00');
insert into Trip values(7778,5,'Boeing    ','Singapore                ','London                   ','1900-01-01 22:00:00','1900-01-01 12:00:00');
insert into Trip values(8881,5,'Boeing    ','London                   ','Paris                    ','1900-01-01 03:00:00','1900-01-01 04:00:00');
insert into Trip values(8882,5,'Boeing    ','Paris                    ','London                   ','1900-01-01 22:00:00','1900-01-01 23:00:00');



                                                                                                                                                                                                                                                                 
/*----Pass_in_trip------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Pass_in_trip values(1100,'2003-04-29 00:00:00',1,'1a        ');
insert into Pass_in_trip values(1123,'2003-04-05 00:00:00',3,'2a        ');
insert into Pass_in_trip values(1123,'2003-04-08 00:00:00',1,'4c        ');
insert into Pass_in_trip values(1123,'2003-04-08 00:00:00',6,'4b        ');
insert into Pass_in_trip values(1124,'2003-04-02 00:00:00',2,'2d        ');
insert into Pass_in_trip values(1145,'2003-04-05 00:00:00',3,'2c        ');
insert into Pass_in_trip values(1181,'2003-04-01 00:00:00',1,'1a        ');
insert into Pass_in_trip values(1181,'2003-04-01 00:00:00',6,'1b        ');
insert into Pass_in_trip values(1181,'2003-04-01 00:00:00',8,'3c        ');
insert into Pass_in_trip values(1181,'2003-04-13 00:00:00',5,'1b        ');
insert into Pass_in_trip values(1182,'2003-04-13 00:00:00',5,'4b        ');
insert into Pass_in_trip values(1187,'2003-04-14 00:00:00',8,'3a        ');
insert into Pass_in_trip values(1188,'2003-04-01 00:00:00',8,'3a        ');
insert into Pass_in_trip values(1182,'2003-04-13 00:00:00',9,'6d        ');
insert into Pass_in_trip values(1145,'2003-04-25 00:00:00',5,'1d        ');
insert into Pass_in_trip values(1187,'2003-04-14 00:00:00',10,'3d        ');
insert into Pass_in_trip values(8882,'2005-11-06 00:00:00',37,'1a        '); 
insert into Pass_in_trip values(7771,'2005-11-07 00:00:00',37,'1c        '); 
insert into Pass_in_trip values(7772,'2005-11-07 00:00:00',37,'1a        '); 
insert into Pass_in_trip values(8881,'2005-11-08 00:00:00',37,'1d        '); 
insert into Pass_in_trip values(7778,'2005-11-05 00:00:00',10,'2a        '); 
insert into Pass_in_trip values(7772,'2005-11-29 00:00:00',10,'3a        ');
insert into Pass_in_trip values(7771,'2005-11-04 00:00:00',11,'4a        ');
insert into Pass_in_trip values(7771,'2005-11-07 00:00:00',11,'1b        ');
insert into Pass_in_trip values(7771,'2005-11-09 00:00:00',11,'5a        ');
insert into Pass_in_trip values(7772,'2005-11-07 00:00:00',12,'1d        ');
insert into Pass_in_trip values(7773,'2005-11-07 00:00:00',13,'2d        ');
insert into Pass_in_trip values(7772,'2005-11-29 00:00:00',13,'1b        ');
insert into Pass_in_trip values(8882,'2005-11-13 00:00:00',14,'3d        ');
insert into Pass_in_trip values(7771,'2005-11-14 00:00:00',14,'4d        ');
insert into Pass_in_trip values(7771,'2005-11-16 00:00:00',14,'5d        ');
insert into Pass_in_trip values(7772,'2005-11-29 00:00:00',14,'1c        ');


DROP DATABASE IF EXISTS computer;
CREATE DATABASE computer;
USE computer;

drop table IF EXISTS Laptop;
drop table IF EXISTS pc;
drop table IF EXISTS printer;
drop table IF EXISTS product;

CREATE TABLE Laptop (
	code int NOT NULL ,
	model varchar (50) NOT NULL ,
	speed smallint NOT NULL ,
	ram smallint NOT NULL ,
	hd real NOT NULL ,
	price decimal(12,2) NULL ,
	screen tinyint NOT NULL 
); 

CREATE TABLE PC (
	code int NOT NULL ,
	model varchar (50) NOT NULL ,
	speed smallint NOT NULL ,
	ram smallint NOT NULL ,
	hd real NOT NULL ,
	cd varchar (10) NOT NULL ,
	price decimal(12,2) NULL 
); 


CREATE TABLE Product (
	maker varchar (10) NOT NULL ,
	model varchar (50) NOT NULL ,
	type varchar (50) NOT NULL 
); 


CREATE TABLE Printer (
	code int NOT NULL ,
	model varchar (50) NOT NULL ,
	color char (1) NOT NULL ,
	`type` varchar (10) NOT NULL ,
	price decimal(12,2) NULL 
); 


ALTER TABLE Laptop ADD 
	CONSTRAINT PK_Laptop PRIMARY KEY 
	(
		code
	);   


ALTER TABLE PC ADD 
	CONSTRAINT PK_pc PRIMARY KEY 
	(
		code
	);   


ALTER TABLE Product ADD 
	CONSTRAINT PK_product PRIMARY KEY 
	(
		model
	);   


ALTER TABLE Printer ADD 
	CONSTRAINT PK_printer PRIMARY KEY 
	(
		code
	);   


ALTER TABLE Laptop ADD 
	CONSTRAINT FK_Laptop_product FOREIGN KEY 
	(
		model
	) REFERENCES Product (
		model
	);


ALTER TABLE PC ADD 
	CONSTRAINT FK_pc_product FOREIGN KEY 
	(
		model
	) REFERENCES Product (
		model
	);


ALTER TABLE Printer ADD 
	CONSTRAINT FK_printer_product FOREIGN KEY 
	(
		model
	) REFERENCES Product (
		model
	);

/*----Product------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Product values('B','1121','PC')
,('A','1232','PC')
,('A','1233','PC')
,('E','1260','PC')
,('A','1276','Printer')
,('D','1288','Printer')
,('A','1298','Laptop')
,('C','1321','Laptop')
,('A','1401','Printer')
,('A','1408','Printer')
,('D','1433','Printer')
,('E','1434','Printer')
,('B','1750','Laptop')
,('A','1752','Laptop')
,('E','2113','PC')
,('E','2112','PC');


                                                                                                                                                                                                                                                                 
/*----PC------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into PC values(1,'1232',500,64,5,'12x',600)
,(2,'1121',750,128,14,'40x',850)
,(3,'1233',500,64,5,'12x',600)
,(4,'1121',600,128,14,'40x',850)
,(5,'1121',600,128,8,'40x',850)
,(6,'1233',750,128,20,'50x',950)
,(7,'1232',500,32,10,'12x',400)
,(8,'1232',450,64,8,'24x',350)
,(9,'1232',450,32,10,'24x',350)
,(10,'1260',500,32,10,'12x',350)
,(11,'1233',900,128,40,'40x',980)
,(12,'1233',800,128,20,'50x',970)
;

                                                                                                                                                                                                                                                                 
/*----Laptop------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Laptop values(1,'1298',350,32,4,700,11)
,(2,'1321',500,64,8,970,12)
,(3,'1750',750,128,12,1200,14)
,(4,'1298',600,64,10,1050,15)
,(5,'1752',750,128,10,1150,14)
,(6,'1298',450,64,10,950,12)
;


                                                                                                                                                                                                                                                                 
/*----Printer------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Printer values(1,'1276','n','Laser',400)
,(2,'1433','y','Jet',270)
,(3,'1434','y','Jet',290)
,(4,'1401','n','Matrix',150)
,(5,'1408','n','Matrix',270)
,(6,'1288','n','Laser',400)
;

DROP DATABASE IF EXISTS inc_out;
CREATE DATABASE inc_out;
USE inc_out;

CREATE TABLE Income (
	code int NOT NULL ,
	point tinyint NOT NULL ,
	date datetime NOT NULL ,
	inc decimal(12,2) NOT NULL 
);


CREATE TABLE Outcome (
	code int NOT NULL ,
	point tinyint NOT NULL ,
	date datetime NOT NULL ,
	`out` decimal(12,2) NOT NULL 
); 


CREATE TABLE Income_o (
	point tinyint NOT NULL ,
	date datetime NOT NULL ,
	inc decimal(12,2) NOT NULL 
); 


CREATE TABLE Outcome_o (
	point tinyint NOT NULL ,
	date datetime NOT NULL ,
	`out` decimal(12,2) NOT NULL 
); 


ALTER TABLE Income  ADD 
	CONSTRAINT PK_Income PRIMARY KEY  NONCLUSTERED 
	(
		code
	);   


ALTER TABLE Outcome  ADD 
	CONSTRAINT PK_Outcome PRIMARY KEY  NONCLUSTERED 
	(
		code
	)   ;


ALTER TABLE Income_o  ADD 
	CONSTRAINT PK_Income_o PRIMARY KEY  NONCLUSTERED 
	(
		point,
		date
	)   ;


ALTER TABLE Outcome_o  ADD 
	CONSTRAINT PK_Outcome_o PRIMARY KEY  NONCLUSTERED 
	(
		point,
		date
	)   ;

                                                                                                                                                                                                                                                                 
/*----Income------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Income values(1,1,'2001-03-22 00:00:00',15000.00);
insert into Income values(2,1,'2001-03-23 00:00:00',15000.00);
insert into Income values(3,1,'2001-03-24 00:00:00',3600.00);
insert into Income values(4,2,'2001-03-22 00:00:00',10000.00);
insert into Income values(5,2,'2001-03-24 00:00:00',1500.00);
insert into Income values(6,1,'2001-04-13 00:00:00',5000.00);
insert into Income values(7,1,'2001-05-11 00:00:00',4500.00);
insert into Income values(8,1,'2001-03-22 00:00:00',15000.00);
insert into Income values(9,2,'2001-03-24 00:00:00',1500.00);
insert into Income values(10,1,'2001-04-13 00:00:00',5000.00);
insert into Income values(11,1,'2001-03-24 00:00:00',3400.00);
insert into Income values(12,3,'2001-09-13 00:00:00',1350.00);
insert into Income values(13,3,'2001-09-13 00:00:00',1750.00);



                                                                                                                                                                                                                                                                 
/* Outcome */
insert into Outcome values(1,1,'2001-03-14 00:00:00',15348.00);
insert into Outcome values(2,1,'2001-03-24 00:00:00',3663.00);
insert into Outcome values(3,1,'2001-03-26 00:00:00',1221.00);
insert into Outcome values(4,1,'2001-03-28 00:00:00',2075.00);
insert into Outcome values(5,1,'2001-03-29 00:00:00',2004.00);
insert into Outcome values(6,1,'2001-04-11 00:00:00',3195.04);
insert into Outcome values(7,1,'2001-04-13 00:00:00',4490.00);
insert into Outcome values(8,1,'2001-04-27 00:00:00',3110.00);
insert into Outcome values(9,1,'2001-05-11 00:00:00',2530.00);
insert into Outcome values(10,2,'2001-03-22 00:00:00',1440.00);
insert into Outcome values(11,2,'2001-03-29 00:00:00',7848.00);
insert into Outcome values(12,2,'2001-04-02 00:00:00',2040.00);
insert into Outcome values(13,1,'2001-03-24 00:00:00',3500.00);
insert into Outcome values(14,2,'2001-03-22 00:00:00',1440.00);
insert into Outcome values(15,1,'2001-03-29 00:00:00',2006.00);
insert into Outcome values(16,3,'2001-09-13 00:00:00',1200.00);
insert into Outcome values(17,3,'2001-09-13 00:00:00',1500.00);
insert into Outcome values(18,3,'2001-09-14 00:00:00',1150.00);



                                                                                                                                                                                                                                                                 
/* Income_o */
insert into Income_o values(1,'2001-03-22 00:00:00',15000.00);
insert into Income_o values(1,'2001-03-23 00:00:00',15000.00);
insert into Income_o values(1,'2001-03-24 00:00:00',3400.00);
insert into Income_o values(1,'2001-04-13 00:00:00',5000.00);
insert into Income_o values(1,'2001-05-11 00:00:00',4500.00);
insert into Income_o values(2,'2001-03-22 00:00:00',10000.00);
insert into Income_o values(2,'2001-03-24 00:00:00',1500.00);
insert into Income_o values(3,'2001-09-13 00:00:00',11500.00);
insert into Income_o values(3,'2001-10-02 00:00:00',18000.00);

                                                                                                                                                                                                                                                                 
/* Outcome_o  */
insert into Outcome_o values(1,'2001-03-14 00:00:00',15348.00);
insert into Outcome_o values(1,'2001-03-24 00:00:00',3663.00);
insert into Outcome_o values(1,'2001-03-26 00:00:00',1221.00);
insert into Outcome_o values(1,'2001-03-28 00:00:00',2075.00);
insert into Outcome_o values(1,'2001-03-29 00:00:00',2004.00);
insert into Outcome_o values(1,'2001-04-11 00:00:00',3195.04);
insert into Outcome_o values(1,'2001-04-13 00:00:00',4490.00);
insert into Outcome_o values(1,'2001-04-27 00:00:00',3110.00);
insert into Outcome_o values(1,'2001-05-11 00:00:00',2530.00);
insert into Outcome_o values(2,'2001-03-22 00:00:00',1440.00);
insert into Outcome_o values(2,'2001-03-29 00:00:00',7848.00);
insert into Outcome_o values(2,'2001-04-02 00:00:00',2040.00);
insert into Outcome_o values(3,'2001-09-13 00:00:00',1500.00);
insert into Outcome_o values(3,'2001-09-14 00:00:00',2300.00);
insert into Outcome_o values(3,'2002-09-16 00:00:00',2150.00);


DROP DATABASE IF EXISTS painiting; 
CREATE DATABASE painiting;
USE painiting;

CREATE TABLE utB (
	B_DATETIME datetime NOT NULL ,
	B_Q_ID int NOT NULL ,
	B_V_ID int NOT NULL ,
	B_VOL tinyint UNSIGNED NOT NULL 
); 


CREATE TABLE utQ (
	Q_ID int NOT NULL ,
	Q_NAME varchar (35) NOT NULL 
); 


CREATE TABLE utV (
	V_ID int NOT NULL ,
	V_NAME varchar (35) NOT NULL ,
	V_COLOR char (1) NOT NULL 
); 


ALTER TABLE utB  ADD 
	CONSTRAINT PK_utB PRIMARY KEY  NONCLUSTERED 
	(
		B_DATETIME,
		B_Q_ID,
		B_V_ID
	);   


ALTER TABLE utQ  ADD 
	CONSTRAINT PK_utQ PRIMARY KEY  NONCLUSTERED 
	(
		Q_ID
	);   


ALTER TABLE utV  ADD 
	CONSTRAINT PK_utV PRIMARY KEY  NONCLUSTERED 
	(
		V_ID
	);   


ALTER TABLE utB ADD 
	CONSTRAINT FK_utB_utQ FOREIGN KEY 
	(
		B_Q_ID
	) REFERENCES utQ (
		Q_ID
	);
    
ALTER TABLE utB ADD 
	CONSTRAINT FK_utB_utV FOREIGN KEY 
	(
		B_V_ID
	) REFERENCES utV (
		V_ID
	);

                                                                                                                                                                                                                                                                
/*----utQ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into utQ values(1,'Square # 01');
insert into utQ values(2,'Square # 02');
insert into utQ values(3,'Square # 03');
insert into utQ values(4,'Square # 04');
insert into utQ values(5,'Square # 05');
insert into utQ values(6,'Square # 06');
insert into utQ values(7,'Square # 07');
insert into utQ values(8,'Square # 08');
insert into utQ values(9,'Square # 09');
insert into utQ values(10,'Square # 10');
insert into utQ values(11,'Square # 11');
insert into utQ values(12,'Square # 12');
insert into utQ values(13,'Square # 13');
insert into utQ values(14,'Square # 14');
insert into utQ values(15,'Square # 15');
insert into utQ values(16,'Square # 16');
insert into utQ values(17,'Square # 17');
insert into utQ values(18,'Square # 18');
insert into utQ values(19,'Square # 19');
insert into utQ values(20,'Square # 20');
insert into utQ values(21,'Square # 21');
insert into utQ values(22,'Square # 22');
insert into utQ values(23,'Square # 23');
insert into utQ values(25,'Square # 25');


                                                                                                                                                                                                                                                                
/*----utV------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into utV values(1,'Balloon # 01','R');
insert into utV values(2,'Balloon # 02','R');
insert into utV values(3,'Balloon # 03','R');
insert into utV values(4,'Balloon # 04','G');
insert into utV values(5,'Balloon # 05','G');
insert into utV values(6,'Balloon # 06','G');
insert into utV values(7,'Balloon # 07','B');
insert into utV values(8,'Balloon # 08','B');
insert into utV values(9,'Balloon # 09','B');
insert into utV values(10,'Balloon # 10','R');
insert into utV values(11,'Balloon # 11','R');
insert into utV values(12,'Balloon # 12','R');
insert into utV values(13,'Balloon # 13','G');
insert into utV values(14,'Balloon # 14','G');
insert into utV values(15,'Balloon # 15','B');
insert into utV values(16,'Balloon # 16','B');
insert into utV values(17,'Balloon # 17','R');
insert into utV values(18,'Balloon # 18','G');
insert into utV values(19,'Balloon # 19','B');
insert into utV values(20,'Balloon # 20','R');
insert into utV values(21,'Balloon # 21','G');
insert into utV values(22,'Balloon # 22','B');
insert into utV values(23,'Balloon # 23','R');
insert into utV values(24,'Balloon # 24','G');
insert into utV values(25,'Balloon # 25','B');
insert into utV values(26,'Balloon # 26','B');
insert into utV values(27,'Balloon # 27','R');
insert into utV values(28,'Balloon # 28','G');
insert into utV values(29,'Balloon # 29','R');
insert into utV values(30,'Balloon # 30','G');
insert into utV values(31,'Balloon # 31','R');
insert into utV values(32,'Balloon # 32','G');
insert into utV values(33,'Balloon # 33','B');
insert into utV values(34,'Balloon # 34','R');
insert into utV values(35,'Balloon # 35','G');
insert into utV values(36,'Balloon # 36','B');
insert into utV values(37,'Balloon # 37','R');
insert into utV values(38,'Balloon # 38','G');
insert into utV values(39,'Balloon # 39','B');
insert into utV values(40,'Balloon # 40','R');
insert into utV values(41,'Balloon # 41','R');
insert into utV values(42,'Balloon # 42','G');
insert into utV values(43,'Balloon # 43','B');
insert into utV values(44,'Balloon # 44','R');
insert into utV values(45,'Balloon # 45','G');
insert into utV values(46,'Balloon # 46','B');
insert into utV values(47,'Balloon # 47','B');
insert into utV values(48,'Balloon # 48','G');
insert into utV values(49,'Balloon # 49','R');
insert into utV values(50,'Balloon # 50','G');
insert into utV values(51,'Balloon # 51','B');
insert into utV values(52,'Balloon # 52','R');
insert into utV values(53,'Balloon # 53','G');
insert into utV values(54,'Balloon # 54','B');
                                                                                                                                                                                                                                                                 
/*----utB------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into utB values('2003-01-01 01:12:01',1,1,155);
insert into utB values('2003-06-23 01:12:02',1,1,100);
insert into utB values('2003-01-01 01:12:03',2,2,255);
insert into utB values('2003-01-01 01:12:04',3,3,255);
insert into utB values('2003-01-01 01:12:05',1,4,255);
insert into utB values('2003-01-01 01:12:06',2,5,255);
insert into utB values('2003-01-01 01:12:07',3,6,255);
insert into utB values('2003-01-01 01:12:08',1,7,255);
insert into utB values('2003-01-01 01:12:09',2,8,255);
insert into utB values('2003-01-01 01:12:10',3,9,255);
insert into utB values('2003-01-01 01:12:11',4,10,50);
insert into utB values('2003-01-01 01:12:12',5,11,100);
insert into utB values('2003-01-01 01:12:13',5,12,155);
insert into utB values('2003-01-01 01:12:14',5,13,155);
insert into utB values('2003-01-01 01:12:15',5,14,100);
insert into utB values('2003-01-01 01:12:16',5,15,50);
insert into utB values('2003-01-01 01:12:17',5,16,205);
insert into utB values('2003-01-01 01:12:18',6,10,155);
insert into utB values('2003-01-01 01:12:19',6,17,100);
insert into utB values('2003-01-01 01:12:20',6,18,255);
insert into utB values('2003-01-01 01:12:21',6,19,255);
insert into utB values('2003-01-01 01:12:22',7,17,155);
insert into utB values('2003-01-01 01:12:23',7,20,100);
insert into utB values('2003-01-01 01:12:24',7,21,255);
insert into utB values('2003-01-01 01:12:25',7,22,255);
insert into utB values('2003-01-01 01:12:26',8,10,50);
insert into utB values('2003-01-01 01:12:27',9,23,255);
insert into utB values('2003-01-01 01:12:28',9,24,255);
insert into utB values('2003-01-01 01:12:29',9,25,100);
insert into utB values('2003-01-01 01:12:30',9,26,155);
insert into utB values('2003-01-01 01:12:31',10,25,155);
insert into utB values('2003-01-01 01:12:31',10,26,100);
insert into utB values('2003-01-01 01:12:33',10,27,10);
insert into utB values('2003-01-01 01:12:34',10,28,10);
insert into utB values('2003-01-01 01:12:35',10,29,245);
insert into utB values('2003-01-01 01:12:36',10,30,245);
insert into utB values('2003-01-01 01:12:37',11,31,100);
insert into utB values('2003-01-01 01:12:38',11,32,100);
insert into utB values('2003-01-01 01:12:39',11,33,100);
insert into utB values('2003-01-01 01:12:40',11,34,155);
insert into utB values('2003-01-01 01:12:41',11,35,155);
insert into utB values('2003-01-01 01:12:42',11,36,155);
insert into utB values('2003-01-01 01:12:43',12,31,155);
insert into utB values('2003-01-01 01:12:44',12,32,155);
insert into utB values('2003-01-01 01:12:45',12,33,155);
insert into utB values('2003-01-01 01:12:46',12,34,100);
insert into utB values('2003-01-01 01:12:47',12,35,100);
insert into utB values('2003-01-01 01:12:48',12,36,100);
insert into utB values('2003-01-01 01:13:01',4,37,20);
insert into utB values('2003-01-01 01:13:02',8,38,20);
insert into utB values('2003-01-01 01:13:03',13,39,123);
insert into utB values('2003-01-01 01:13:04',14,39,111);
insert into utB values('2003-01-01 01:13:05',14,40,50);
insert into utB values('2003-01-01 01:13:06',15,41,50);
insert into utB values('2003-01-01 01:13:07',15,41,50);
insert into utB values('2003-01-01 01:13:08',15,42,50);
insert into utB values('2003-01-01 01:13:09',15,42,50);
insert into utB values('2003-01-01 01:13:10',16,42,50);
insert into utB values('2003-01-01 01:13:11',16,42,50);
insert into utB values('2003-01-01 01:13:12',16,43,50);
insert into utB values('2003-01-01 01:13:13',16,43,50);
insert into utB values('2003-01-01 01:13:14',16,47,50);
insert into utB values('2003-01-01 01:13:15',17,44,10);
insert into utB values('2003-01-01 01:13:16',17,44,10);
insert into utB values('2003-01-01 01:13:17',17,45,10);
insert into utB values('2003-01-01 01:13:18',17,45,10);
insert into utB values('2003-02-01 01:13:19',18,45,10);
insert into utB values('2003-03-01 01:13:20',18,45,10);
insert into utB values('2003-04-01 01:13:21',18,46,10);
insert into utB values('2003-05-01 01:13:22',18,46,10);
insert into utB values('2003-06-11 01:13:23',19,44,10);
insert into utB values('2003-01-01 01:13:24',19,44,10);
insert into utB values('2003-01-01 01:13:25',19,45,10);
insert into utB values('2003-01-01 01:13:26',19,45,10);
insert into utB values('2003-02-01 01:13:27',20,45,10);
insert into utB values('2003-03-01 01:13:28',20,45,10);
insert into utB values('2003-04-01 01:13:29',20,46,10);
insert into utB values('2003-05-01 01:13:30',20,46,10);
insert into utB values('2003-02-01 01:13:31',21,49,50);
insert into utB values('2003-02-02 01:13:32',21,49,50);
insert into utB values('2003-02-03 01:13:33',21,50,50);
insert into utB values('2003-02-04 01:13:34',21,50,50);
insert into utB values('2003-02-05 01:13:35',21,48,1);
insert into utB values('2000-01-01 01:13:36',22,50,50);
insert into utB values('2001-01-01 01:13:37',22,50,50);
insert into utB values('2002-01-01 01:13:38',22,51,50);
insert into utB values('2002-06-01 01:13:39',22,51,50);
insert into utB values('2003-01-01 01:13:05',4,37,185);



DROP DATABASE IF EXISTS ships;
CREATE DATABASE ships;
USE ships;

drop table IF EXISTS outcomes;
drop table IF EXISTS ships;
drop table IF EXISTS classes;
drop table IF EXISTS battles;

CREATE TABLE Battles (
	name varchar (20) NOT NULL ,
	date datetime NOT NULL 
);


CREATE TABLE Classes (
	class varchar (50) NOT NULL ,
	type varchar (2) NOT NULL ,
	country varchar (20) NOT NULL ,
	numGuns tinyint NULL ,
	bore real NULL ,
	displacement int NULL 
); 


CREATE TABLE Ships (
	name varchar (50) NOT NULL ,
	class varchar (50) NOT NULL ,
	launched smallint NULL 
); 


CREATE TABLE Outcomes (
	ship varchar (50) NOT NULL ,
	battle varchar (20) NOT NULL ,
	result varchar (10) NOT NULL 
); 


ALTER TABLE Battles ADD 
	CONSTRAINT PK_Battles PRIMARY KEY  CLUSTERED 
	(
		name
	)   ;


ALTER TABLE Classes ADD 
	CONSTRAINT PK_Classes PRIMARY KEY  CLUSTERED 
	(
		class
	)   ;


ALTER TABLE Ships ADD 
	CONSTRAINT PK_Ships PRIMARY KEY  CLUSTERED 
	(
		name
	)   ;


ALTER TABLE Outcomes ADD 
	CONSTRAINT PK_Outcomes PRIMARY KEY  CLUSTERED 
	(
		ship,
		battle
	)   ;


ALTER TABLE Ships ADD 
	CONSTRAINT FK_Ships_Classes FOREIGN KEY 
	(
		class
	) REFERENCES Classes (
		class
	) ;


ALTER TABLE Outcomes ADD 
	CONSTRAINT FK_Outcomes_Battles FOREIGN KEY 
	(
		battle
	) REFERENCES Battles (
		name
	);

                                                                                                                                                                                                                                                               
/*----Classes------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Classes values('Bismarck','bb','Germany',8,15,42000);
insert into Classes values('Iowa','bb','USA',9,16,46000);
insert into Classes values('Kongo','bc','Japan',8,14,32000);
insert into Classes values('North Carolina','bb','USA',12,16,37000);
insert into Classes values('Renown','bc','Gt.Britain',6,15,32000);
insert into Classes values('Revenge','bb','Gt.Britain',8,15,29000);
insert into Classes values('Tennessee','bb','USA',12,14,32000);
insert into Classes values('Yamato','bb','Japan',9,18,65000);



                                                                                                                                                                                                                                                                 
/*----Battles------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Battles values('Guadalcanal','1942-11-15 00:00:00');
insert into Battles values('North Atlantic','1941-05-25 00:00:00');
insert into Battles values('North Cape','1943-12-26 00:00:00');
insert into Battles values('Surigao Strait','1944-10-25 00:00:00');
insert into Battles values ('#Cuba62a'   , '1962-10-20');
insert into Battles values ('#Cuba62b'   , '1962-10-25');



                                                                                                                                                                                                                                                                 
/*----Ships------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Ships values('California','Tennessee',1921);
insert into Ships values('Haruna','Kongo',1916);
insert into Ships values('Hiei','Kongo',1914);
insert into Ships values('Iowa','Iowa',1943);
insert into Ships values('Kirishima','Kongo',1915);
insert into Ships values('Kongo','Kongo',1913);
insert into Ships values('Missouri','Iowa',1944);
insert into Ships values('Musashi','Yamato',1942);
insert into Ships values('New Jersey','Iowa',1943);
insert into Ships values('North Carolina','North Carolina',1941);
insert into Ships values('Ramillies','Revenge',1917);
insert into Ships values('Renown','Renown',1916);
insert into Ships values('Repulse','Renown',1916);
insert into Ships values('Resolution','Renown',1916);
insert into Ships values('Revenge','Revenge',1916);
insert into Ships values('Royal Oak','Revenge',1916);
insert into Ships values('Royal Sovereign','Revenge',1916);
insert into Ships values('Tennessee','Tennessee',1920);
insert into Ships values('Washington','North Carolina',1941);
insert into Ships values('Wisconsin','Iowa',1944);
insert into Ships values('Yamato','Yamato',1941);
insert into Ships values('South Dakota','North Carolina',1941); 


                                                                                                                                                                                                                                                         
/*----Outcomes------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
insert into Outcomes values('Bismarck','North Atlantic','sunk');
insert into Outcomes values('California','Surigao Strait','OK');
insert into Outcomes values('Duke of York','North Cape','OK');
insert into Outcomes values('Fuso','Surigao Strait','sunk');
insert into Outcomes values('Hood','North Atlantic','sunk');
insert into Outcomes values('King George V','North Atlantic','OK');
insert into Outcomes values('Kirishima','Guadalcanal','sunk');
insert into Outcomes values('Prince of Wales','North Atlantic','damaged');
insert into Outcomes values('Rodney','North Atlantic','OK');
insert into Outcomes values('Schamhorst','North Cape','sunk');
insert into Outcomes values('South Dakota','Guadalcanal','damaged');
insert into Outcomes values('Tennessee','Surigao Strait','OK');
insert into Outcomes values('Washington','Guadalcanal','OK');
insert into Outcomes values('West Virginia','Surigao Strait','OK');
insert into Outcomes values('Yamashiro','Surigao Strait','sunk');
insert into Outcomes values('California','Guadalcanal','damaged');


