This MariaDB server preset contains DMLs and data for SQL-ex.ru 
excersises.
- Компьютерная фирма
- Корабли
- Фирма вторсырья
- Аэрофлот
- Окраска

docker run -v $(PWD)/var/lib/mysql:/var/lib/mysql --name MariaDB -p 3306:3306 -e MYSQL_ROOT_PASSWORD=qazXSW2~ -d mariadb:latest
docker exec -i MariaDB sh -c 'exec mysql -uroot -p"qazXSW2~"' < init.sql