docker run --rm -v "$(pwd)"/sql:/opt/scripts/ -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=qazXSW2~' \
   -p 1433:1433 --name sql1 \
   -d mcr.microsoft.com/mssql/server:2017-latest

# docker exec -it sql1 bash
# apt-get update && apt-get install -y mssql-tools unixodbc-dev
# docker exec -it <container_id|container_name> /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P <your_password>
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -Q "create database aero" &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -Q "create database computer"  &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -Q "create database inc_out"  &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -Q "create database painting"  &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -Q "create database Ships"

docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -i /opt/scripts/aero_script.sql &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -i /opt/scripts/computer_script.sql &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -i /opt/scripts/inc_out_script.sql &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -i /opt/scripts/painting_script.sql &&
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P qazXSW2~ -i /opt/scripts/ships_script.sql